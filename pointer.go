      //NAMED RETURN VALUES

package main

import "fmt"

func main(){
	son1,son2 := split(10)
	fmt.Println("X:",son1,"Y:",son2)

	l,r :=add(10,20,30,40,50,60,70,80)
	fmt.Println("Uzunligi: ",l,"Yigindisi: ",r)
	
	p,q := example(12,2,34,5)
	fmt.Println("Sigim: ",p,"Yigindi: ",q)

}
func split(sum int) (x, y int){
	x = sum + 20
	y = sum - x
	return x, y
}
func add(nums ...int) (length,result int){
	result = 0
	for _, nums := range nums {
		result +=nums
	}
	length = len(nums)
	return 
}
func example(son ...int) (length, yigindi int){
	yigindi=0
	for _, son := range son {
		yigindi +=son
		
	}
	length=len(son)
	return 

	
}