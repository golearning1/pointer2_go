       //VARIADIC FUNCTION

package main

import "fmt"

func main(){
	yigindi,uzunlik :=yigindiHisobla(10,20,30,40,50,60,65,43,21,100)
    fmt.Println("Yigindi: ",yigindi,"Uzunlik: ",uzunlik)

	kopaytma,length :=fak(10,10,2)
	fmt.Println("Ko'paytma: ",kopaytma,"Uzunligi: ",length)

}
func yigindiHisobla(son ...int) (yigindi,length int){
	yigindi = 0
	
	for _, son := range son {
		yigindi +=son
	}
	length=len(son)
	return
}

func fak(sum ... int)(kopaytma,length int){
	kopaytma = 1
	for _,sum := range sum {
		kopaytma *=sum
	}
	length=len(sum)
	return
}

	